FROM debian:stretch

MAINTAINER morph027 "morphsen@gmx.com"

ADD https://git.samba.org/?p=samba.git;a=blob_plain;f=bootstrap/generated-dists/debian9/bootstrap.sh;hb=v4-12-test /usr/local/bin/samba-bootstrap.sh

RUN apt-get update \
    && apt-get -y install \
	  ruby \
	  ruby-dev \
	  ruby-ffi \
	&& bash /usr/local/bin/samba-bootstrap.sh \
	&& rm -rf /var/lib/apt/lists/*

RUN	gem install fpm --no-doc \
	&& gem cleanup all
