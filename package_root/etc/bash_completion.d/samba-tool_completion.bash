#!/bin/bash

#   Bash-Completion for samba-tool commands
#   Copyright (C) Stephan Brauer 2015

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.

dbcheck="dbcheck"
delegation="delegation add-service del-service for-any-protocol\
 for-any-service show"
dns="dns add delete query roothints serverinfo update zonecreate\
 zonedelete zoneinfo zonelist"
domain="domain assicupgrade dcpromo demote exportkeytab info join\
 level passwordsettings provision"
drs="drs bind kcc options replicate showrepl"
dsacl="dsacl set"
fsmo="fsmo seize show transfer"
gpo="gpo aclcheck create del dellink fetch getinheritance getlink list\
 listall listcontainers setinheritance setlink show"
group="group add addmembers delete list listmembers removemembers"
ldapcmp="ldapcmp"
ntacl="ntacl get set sysvolcheck sysvolreset"
processes="processes"
rodc="rodc preload"
sites="sites create remove"
spn="spn add delete list"
testparm="testparm"
time="time"
user="user add create delete disable enable list password setexpiry\
 setpassword"
vampire="vampire"

complete -W "$dbcheck\
 $deletation\
 $dns\
 $domain\
 $drs\
 $dsacl\
 $fsmo\
 $gpo\
 $group\
 $ldapcmp\
 $ntacl\
 $processes\
 $rodc\
 $sites\
 $spn\
 $testparm\
 $time\
 $user\
 $vampire\
 " -f samba-tool
