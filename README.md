# Build SAMBA4 deb's from source

This project builds SAMBA4 AD DC debs [from source](https://wiki.samba.org/index.php/Build_Samba_from_source) using [fpm](https://github.com/jordansissel/fpm).

At the moment, these are being built for Debian Stretch 9 (amd64).

## Usage

To enable the daemons you need, just issue `systemctl enable samba-ad-dc` and `systemctl start samba-ad-dc`
