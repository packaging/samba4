# https://wiki.samba.org/index.php/Libnss_winbind_Links#x86_64_2
ln -s /var/lib/samba/lib/libnss_winbind.so.2 /lib/x86_64-linux-gnu/
ln -s /lib/x86_64-linux-gnu/libnss_winbind.so.2 /lib/x86_64-linux-gnu/libnss_winbind.so
ldconfig

groupadd -r winbindd_priv

# https://wiki.samba.org/index.php/Managing_the_Samba_AD_DC_Service_Using_Systemd#The_samba_Service
systemctl mask smbd nmbd winbind
systemctl disable smbd nmbd winbind
systemctl daemon-reload
