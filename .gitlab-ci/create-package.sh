#!/bin/bash

set -e

create_package() {
local TYPE="$1"
mkdir -p $CI_PROJECT_DIR/packages
fpm -f -s dir -t $TYPE -n samba4 \
--prefix="/" \
--vendor "SAMBA" \
--maintainer "morph027" \
--version "${CI_COMMIT_TAG}" \
--package ${CI_PROJECT_DIR}/samba4_${CI_COMMIT_TAG}_${ARCH}_${DIST}.deb \
--after-install package_scripts/after-install.sh \
--after-remove package_scripts/after-remove.sh \
--after-upgrade package_scripts/after-upgrade.sh \
--before-remove package_scripts/before-remove.sh \
--before-upgrade package_scripts/before-upgrade.sh \
--depends python-dnspython \
--depends libpython2.7 \
--depends libbsd0 \
--depends libcups2 \
--depends libpopt0 \
--depends bzip2 \
--depends tar \
--depends libldap-2.4-2 \
--depends libjansson4 \
--depends libgpgme11 \
-C ${CI_PROJECT_DIR}/package_root \
etc \
lib \
usr \
var
}

which fpm > /dev/null

if [ $? -eq 0 ]; then
  create_package deb 
else
  echo "fpm not found in PATH"
  exit 1
fi
