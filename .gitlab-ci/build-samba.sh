#!/bin/bash

set -e

download_source() {
  URL=https://download.samba.org/pub/samba/stable/samba-${CI_COMMIT_TAG%%-*}.tar.gz
  echo "downloading $URL"
  curl -s -O "$URL"
}

extract_source() {
  tar xzf "samba-${CI_COMMIT_TAG%%-*}.tar.gz"
}

compile_source() {
  (cd "samba-${CI_COMMIT_TAG%%-*}" && \
  ./configure \
  --prefix=/usr \
  --enable-fhs \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --with-privatedir=/var/lib/samba/private \
  --with-piddir=/var/run/samba \
  --with-pammodulesdir="/lib/${DEB_HOST_MULTIARCH}/security" \
  --libdir="/usr/lib/${DEB_HOST_MULTIARCH}" \
  --with-modulesdir="/usr/lib/${DEB_HOST_MULTIARCH}/samba" \
  --datadir=/usr/share \
  --with-lockdir=/var/run/samba \
  --with-statedir=/var/lib/samba \
  --with-cachedir=/var/cache/samba \
  --enable-gnutls \
  && make \
  && make install DESTDIR="${CI_PROJECT_DIR}/package_root")
}

download_source
extract_source
compile_source
