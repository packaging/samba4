#!/bin/bash

set -e

echo -ne "\n---------- TEST INSTALL ---------------------\n\n"
dpkg -i $CI_PROJECT_DIR/samba4_${CI_BUILD_REF_NAME}_${ARCH}_${DIST}.deb
echo -ne "\n---------- FINISHED -------------------------\n"

echo -ne "\n---------- LIST OF INSTALLED FILED ----------\n\n"
dpkg -L samba4
echo -ne "\n---------- END ------------------------------\n"

echo -ne "\n---------- TEST PROVISION -------------------\n\n"
samba-tool domain provision --adminpass=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1) --domain=FOO.BAR --realm=FOO --use-rfc2307 --use-ntvfs
echo -ne "\n---------- END ------------------------------\n"

echo -ne "\n---------- TEST DAEMON START ----------------\n\n"
/etc/init.d/samba-ad-dc start && \
sleep 10 && \
/etc/init.d/samba-ad-dc stop
echo -ne "\n---------- END ------------------------------\n"
